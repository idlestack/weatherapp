//
//  WeatherVC.swift
//  WeatherApp
//
//  Created by Douglas Spencer on 9/14/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate{

    @IBOutlet weak var lblCurrentDate: UILabel!
    @IBOutlet weak var lblCurrentTemperature: UILabel!
    @IBOutlet weak var lblCurrentType: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var tblvWeatherDays: UITableView!
    @IBOutlet weak var imgCurrentWeatherImage: UIImageView!
    
    @IBOutlet weak var TemperatureType: UISegmentedControl!
    
    let locman = CLLocationManager()
    
    var TempType: String?
    
    var currLocation: CLLocation!
    
        var currentWeather = CurrentWeather()
    var forecasts = [Forecast]()
    var forecast: Forecast!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locman.delegate = self;
        locman.desiredAccuracy = kCLLocationAccuracyBest
        locman.requestWhenInUseAuthorization()
        locman.startMonitoringSignificantLocationChanges()

        tblvWeatherDays.delegate = self;
        tblvWeatherDays.dataSource = self;
        
    }

    override func viewDidAppear(_ animated: Bool) {
        locationAuthStatus()
    }
    @IBAction func TempTypeValueChanged(_ sender: AnyObject, forEvent event: UIEvent) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            TempType = "F"
            break;
        case 1:
            TempType = "C"
            break;
        default:
            TempType = "F"
            break;
        }
    }
    
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            currLocation = locman.location
            
            Location.loc.latt = currLocation.coordinate.latitude
            Location.loc.long = currLocation.coordinate.longitude
            
            print(Location.loc.latt)
            print(Location.loc.long)
            
            currentWeather.downloadWeatherDetails {
                self.updateUI()
                self.downloadForeCastdata {
                }
            }

            print ( currLocation)
            
            
        } else {
            locationAuthStatus()
        }
    }
    
    func downloadForeCastdata(_ complete: DownloadComplete) {
        let forecastURL = URL(string: CURRENT_FORECAST_URL)!
        
        Alamofire.request(forecastURL, method: .get).responseJSON{ response in
            let result = response.result
            print(response)
            
            if let dict = result.value as? Dictionary<String,Any> {
                if let list = dict["list"] as? [Dictionary<String,AnyObject>] {
                    for obj in list {
                        let forecast = Forecast(weatherDict: obj)
                        self.forecasts.append(forecast)
                        print(obj)
                    }
                    self.tblvWeatherDays.reloadData()
                }
            }
    }
        complete()
}
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecasts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tblvWeatherDays.dequeueReusableCell(withIdentifier: "ReusableTableCell", for: indexPath) as? WeatherCellTableViewCell {
            
            let forecast = forecasts[indexPath.row]
            cell.ConfigureCell(forecast)
            return cell;
        } else {
            return WeatherCellTableViewCell()
        }
    }
    
    func updateUI(){
        lblCurrentDate.text = currentWeather.date
        lblLocation.text = currentWeather.CityName
        lblCurrentType.text = currentWeather.WeatherType
        lblCurrentTemperature.text = "\(currentWeather.CurrentTemp)"
        imgCurrentWeatherImage.image = UIImage(named: currentWeather.WeatherType)
    }
}

