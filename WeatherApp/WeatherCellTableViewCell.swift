//
//  WeatherCellTableViewCell.swift
//  WeatherApp
//
//  Created by Douglas Spencer on 9/15/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit

class WeatherCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var LowTemp: UILabel!
    @IBOutlet weak var HighTemp: UILabel!
    @IBOutlet weak var WeatherType: UILabel!
    @IBOutlet weak var DayLabel: UILabel!
    
    func ConfigureCell(_ forecast: Forecast) {
        LowTemp.text = "\(forecast.lowTemp)"
        HighTemp.text = "\(forecast.HighTemp)"
        WeatherType.text = forecast.weatherType
        DayLabel.text = forecast.date
        weatherIcon.image = UIImage(named: forecast.weatherType)
        self.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }

}
