//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by Douglas Spencer on 9/14/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    var _cityName: String!
    var _date: String!
    var _weatherType: String!
    var _CurrentTemp: Double!
    
    var CityName : String {
        if ( _cityName == nil) {
            _cityName = ""
        }
        
        return _cityName
    }
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        
        let currentDate = dateFormatter.string(from: Date())
        
        self._date = "Today, \(currentDate)"
        
        return _date;
    }
    
    var WeatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        
        return _weatherType
    }
    
    var CurrentTemp: Double {
        if _CurrentTemp == nil {
            _CurrentTemp = 0.0
        }
        
        return _CurrentTemp
    }
    
    func downloadWeatherDetails(_ completed: @escaping DownloadComplete) {
        let CurrentWeatherURL = URL(string: CURRENT_WEATHER_URL)!
        Alamofire.request(CurrentWeatherURL, method: .get).responseJSON { response in
        
            let result = response.result
            
            if let dict = result.value as? Dictionary<String,AnyObject> {
                
                if let name = dict["name"] as? String {
                    self._cityName = name.capitalized
                    print(self.CityName)
                }
                
                if let weathertype = dict["weather"] as?  [Dictionary<String,AnyObject>]{
                    if let main = weathertype[0]["main"] as? String {
                        self._weatherType = main.capitalized
                        print(self.WeatherType)
                    }
                }
            
                if let currenttemp = dict["main"] as? Dictionary<String,AnyObject> {
                    if let temp = currenttemp["temp"] as? Double {
                        let kelvinToFarendivisor = (temp * (9/5) - 459.67)
                        let FarenTemp = Double(round(10 * kelvinToFarendivisor/10))
                        self._CurrentTemp = FarenTemp
                        print(self._CurrentTemp)
                    }
                }
            }
            
            completed()
        }
    }
}
