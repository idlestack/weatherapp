//
//  Forecast.swift
//  WeatherApp
//
//  Created by Douglas Spencer on 9/15/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit
import Alamofire

class Forecast {
    var _date :String!
    var _weatherType :String!
    var _HighTemp: Double!
    var _LowTemp: Double!
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        
        return _weatherType
    }
    
    var HighTemp :Double {
        if _HighTemp == nil {
            _HighTemp = 0.0
        }
        
        return _HighTemp
    }
    
    var lowTemp :Double {
        if _LowTemp == nil {
            _LowTemp = 0.0
        }
        
        return _LowTemp
    }
    
    init(weatherDict: Dictionary<String, AnyObject>) {
        if let temp = weatherDict["temp"] as? Dictionary<String,Any> {
            if let min = temp["min"] as? Double {
                let kelvinToFarendivisor = (min * (9/5) - 459.67)
                let FarenTemp = Double(round(10 * kelvinToFarendivisor/10))
                self._LowTemp = FarenTemp
            }
            
            if let max = temp["max"] as? Double {
                let kelvinToFarendivisor = (max * (9/5) - 459.67)
                let FarenTemp = Double(round(10 * kelvinToFarendivisor/10))
                self._HighTemp = FarenTemp
            }
        }
        
        if let weather = weatherDict["weather"] as? [Dictionary<String,Any>] {
            if let main = weather[0]["main"] as? String {
                self._weatherType = main
            }
        }
        
        if let date = weatherDict["dt"] as? Double {
            let unixConvertedDate = Date(timeIntervalSince1970: date)
            let dtf = DateFormatter()
            dtf.dateStyle = .full
            dtf.dateFormat = "EEEE"
            dtf.timeStyle = .none
            self._date = unixConvertedDate.dayOfTheWeek()
        }
    }
    
}

extension Date {
    func dayOfTheWeek() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}
