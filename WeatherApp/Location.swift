//
//  Location.swift
//  WeatherApp
//
//  Created by Douglas Spencer on 9/18/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import Foundation
import CoreLocation

class Location {
    static var loc = Location()
    fileprivate init() {}
    
    var latt: Double!
    var long: Double!
}
