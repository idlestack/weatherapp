//
//  Constants.swift
//  WeatherApp
//
//  Created by Douglas Spencer on 9/14/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import Foundation

let API_KEY = "&appid=89f691a897d2420db5b0b71d99238920"
let BASE_API_URL = "http://api.openweathermap.org/data/2.5/weather?"
let FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?"
let LATT = "lat=35"
let LONG = "&lon=139"

let CURRENT_WEATHER_URL = "\(BASE_API_URL)lat=\(Location.loc.latt!)&lon=\(Location.loc.long!)\(API_KEY)"
let CURRENT_FORECAST_URL = "\(FORECAST_URL)lat=\(Location.loc.latt!)&lon=\(Location.loc.long!)\(API_KEY)"

typealias DownloadComplete = () -> ()

var WEATHERFACTS = [String]()




